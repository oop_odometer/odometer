public class Odometer {
    int size;
    String digits = "123456789";
    int START;
    int LIMIT;
    int reading;
    
    public Odometer(int size, int initReading) {
        this.size = size;
        reading = initReading;
        START = Integer.parseInt(digits.substring(0, size));
        LIMIT = Integer.parseInt(digits.substring(9 - size));
    }
    
    public int next() {
        if (reading == LIMIT) {
            return START;
        }
        ++reading;
        if (Odometer.isAscending(reading)) {
            return reading;
        } else {
            return next(reading);
        }
    }

    public static boolean isAscending(int n) {
        if (n < 10)
            return true;
        if (n % 10 <= (n / 10) % 10) {
            return false;
        }
        return isAscending(n / 10);
    }
    
}

class ObjectOdometer {
    public static void main(String[] args) {
        if(!Odometer.isAscending(Integer.parseInt(args[1]))) {
            System.exit(0);
        }
        Odometer odometer1 = new Odometer(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        odometer1.next()
    }
}